use egui_nodes::ColorStyle;

pub fn dark_colors() -> [egui::Color32; ColorStyle::Count as usize] {
    let mut colors = [egui::Color32::RED; ColorStyle::Count as usize];
    colors[ColorStyle::NodeBackground as usize] =
        egui::Color32::from_rgba_unmultiplied(30, 30, 30, 255);
    colors[ColorStyle::NodeBackgroundHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(50, 50, 50, 255);
    colors[ColorStyle::NodeBackgroundSelected as usize] =
        egui::Color32::from_rgba_unmultiplied(50, 50, 50, 255);
    colors[ColorStyle::NodeOutline as usize] = egui::Color32::from_rgba_unmultiplied(0, 0, 0, 0);
    colors[ColorStyle::TitleBar as usize] = egui::Color32::from_rgba_unmultiplied(41, 74, 122, 255);
    colors[ColorStyle::TitleBarHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(66, 150, 250, 255);
    colors[ColorStyle::TitleBarSelected as usize] =
        egui::Color32::from_rgba_unmultiplied(66, 150, 250, 255);
    colors[ColorStyle::Link as usize] = egui::Color32::from_rgba_unmultiplied(61, 133, 224, 200);
    colors[ColorStyle::LinkHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(66, 150, 250, 255);
    colors[ColorStyle::LinkSelected as usize] =
        egui::Color32::from_rgba_unmultiplied(66, 150, 250, 255);
    colors[ColorStyle::Pin as usize] = egui::Color32::from_rgba_unmultiplied(53, 150, 250, 180);
    colors[ColorStyle::PinHovered as usize] =
        egui::Color32::from_rgba_unmultiplied(53, 150, 250, 255);
    colors[ColorStyle::BoxSelector as usize] =
        egui::Color32::from_rgba_unmultiplied(61, 133, 224, 30);
    colors[ColorStyle::BoxSelectorOutline as usize] =
        egui::Color32::from_rgba_unmultiplied(61, 133, 224, 150);
    colors[ColorStyle::GridBackground as usize] =
        egui::Color32::from_rgba_unmultiplied(10, 10, 10, 200);
    colors[ColorStyle::GridLine as usize] = egui::Color32::from_rgba_unmultiplied(50, 50, 50, 40);
    colors
}
