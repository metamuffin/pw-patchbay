use egui::{Key, Ui};

use crate::{pwconn::MessageIn, App};

impl App {
    pub fn patchbay_inner(&mut self, ui: &mut Ui) {
        let canvas = self.ctx.show(
            self.nodes.values().map(|e| e.node_constructor(&self.ports)),
            self.links
                .values()
                .map(|e| e.link_constructor(&self.nodes, &self.ports)),
            ui,
        );

        canvas.context_menu(|ui| {
            let nlinks = self.ctx.get_selected_links().len();
            let nnodes = self.ctx.get_selected_nodes().len();
            if nlinks > 0
                && ui
                    .button(format!("Destroy selected links ({nlinks})",))
                    .clicked()
            {
                self.delete_selected_links();
                ui.close_menu()
            }
            if nlinks > 0
                && ui
                    .button(format!("Destroy selected nodes ({nnodes})"))
                    .clicked()
            {
                ui.close_menu()
            }
            ui.menu_button("Add node", |ui| {
                ui.label("todo");
            });
        });

        if let Some(idx) = self.ctx.link_destroyed() {
            self.links.remove(&idx);
        }

        if ui.input().key_pressed(Key::Delete) {
            self.delete_selected_links()
        }

        if let Some((input_port, output_port, _)) = self.ctx.link_created() {
            let input_node = self.ports.get(&input_port).unwrap().node;
            let output_node = self.ports.get(&output_port).unwrap().node;
            self.message_out
                .send(MessageIn::CreateLink {
                    input_node,
                    input_port,
                    output_node,
                    output_port,
                })
                .unwrap();
        }
    }
}
