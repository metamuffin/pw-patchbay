use std::collections::HashMap;

use egui::Ui;
use egui_nodes::{NodeArgs, NodeConstructor, PinArgs};
use pipewire::spa::Direction;

use crate::port::PortWrap;

#[derive(Debug)]
pub enum NodeType {
    Input,
    Output,
    Unknown,
}
#[derive(Debug)]
pub enum MediaType {
    Audio,
    Video,
    Midi,
    Unknown,
}

#[derive(Debug)]
pub struct NodeWrap {
    pub id: usize,
    // pub ui_attr_id: usize,
    pub name: String,
    pub application: Option<String>,
    pub ports: Vec<usize>,
    pub kind: NodeType,
    pub media_type: MediaType,
}

impl NodeWrap {
    pub fn node_constructor<'a>(
        &'a self,
        ports: &'a HashMap<usize, PortWrap>,
    ) -> NodeConstructor<'a> {
        let color = self.media_type.accent_color();

        let mut cons = NodeConstructor::new(
            self.id,
            NodeArgs {
                titlebar: Some(color),
                ..Default::default()
            },
        )
        .with_origin([rand::random::<f32>() * 500.0, rand::random::<f32>() * 500.0].into())
        .with_title(move |ui| {
            ui.label(&if let Some(a) = &self.application {
                format!("{a}: {}", self.name)
            } else {
                format!("{}", self.name)
            })
        });
        // .with_static_attribute(self.ui_attr_id, |ui| ui.label(&self.application));

        for pid in &self.ports {
            let p = ports.get(&pid).unwrap();
            let ui = |ui: &mut Ui| ui.label(&p.name);
            let args = PinArgs {
                background: Some(color),
                ..Default::default()
            };
            match p.direction {
                Direction::Input => cons = cons.with_input_attribute(p.id, args, ui),
                Direction::Output => cons = cons.with_output_attribute(p.id, args, ui),
            }
        }
        cons
    }
}

impl MediaType {
    pub fn accent_color(&self) -> egui::Color32 {
        match self {
            MediaType::Audio => egui::Color32::from_rgb(50, 50, 150),
            MediaType::Midi => egui::Color32::from_rgb(50, 150, 50),
            MediaType::Video => egui::Color32::from_rgb(150, 50, 50),
            MediaType::Unknown => egui::Color32::from_rgb(70, 70, 70),
        }
    }
}
