use crate::{node::NodeWrap, port::PortWrap};
use egui_nodes::LinkArgs;
use pipewire::link::LinkState;
use std::collections::HashMap;

#[derive(Debug)]
pub enum SimpleLinkState {
    Active,
    Paused,
    Unknown,
}

#[derive(Debug)]
pub struct LinkWrap {
    pub id: usize,
    pub start: usize,
    pub end: usize,
    pub state: SimpleLinkState,
}

impl LinkWrap {
    pub fn link_constructor(
        &self,
        nodes: &HashMap<usize, NodeWrap>,
        ports: &HashMap<usize, PortWrap>,
    ) -> (usize, usize, usize, LinkArgs) {
        (
            self.id,
            self.start,
            self.end,
            LinkArgs {
                base: ports
                    .get(&self.start)
                    .and_then(|port| nodes.get(&port.node))
                    .map(|n| {
                        n.media_type
                            .accent_color()
                            .linear_multiply(match self.state {
                                SimpleLinkState::Active => 1.0,
                                SimpleLinkState::Paused => 0.2,
                                SimpleLinkState::Unknown => 0.2,
                            })
                    }),
                hovered: None,
                selected: None,
                ..Default::default()
            },
        )
    }
}

impl From<LinkState<'_>> for SimpleLinkState {
    fn from(l: LinkState) -> Self {
        match l {
            LinkState::Error(_) => Self::Unknown,
            LinkState::Unlinked => Self::Unknown,
            LinkState::Init => Self::Unknown,
            LinkState::Negotiating => Self::Unknown,
            LinkState::Allocating => Self::Unknown,
            LinkState::Paused => Self::Paused,
            LinkState::Active => Self::Active,
        }
    }
}
