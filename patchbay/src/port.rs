use pipewire::spa::Direction;


#[derive(Debug)]
pub struct PortWrap {
    pub id: usize,
    pub name: String,
    pub direction: Direction,
    pub node: usize,
}
