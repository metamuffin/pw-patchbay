use crate::{
    link::LinkWrap,
    node::{MediaType, NodeType, NodeWrap},
    port::PortWrap,
};
use crossbeam_channel::{Receiver, Sender};
use pipewire::{
    link::Link,
    prelude::ReadableDict,
    properties,
    spa::{Direction, ForeignDict},
    types::ObjectType,
    Context, Loop, MainLoop,
};
use std::{cell::RefCell, collections::HashMap, rc::Rc, time::Duration};

#[derive(Debug)]
pub enum MessageIn {
    RemoveLink(usize),
    CreateLink {
        input_node: usize,
        input_port: usize,
        output_node: usize,
        output_port: usize,
    },
}

#[derive(Debug)]
pub enum MessageOut {
    AddNode(NodeWrap),
    UpdateLink(LinkWrap),
    AddPort(PortWrap),
    Remove(usize),
}

pub fn pw(
    out_tx: Sender<MessageOut>,
    in_rx: Receiver<MessageIn>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mainloop = MainLoop::new()?;
    let context = Context::new(&mainloop)?;
    let core = context.connect(None)?;
    let registry = Rc::new(core.get_registry()?);

    let proxies = Rc::new(RefCell::new(HashMap::<usize, _>::new()));

    let registry2 = registry.clone();
    let out_tx2 = out_tx.clone();
    let proxies2 = proxies.clone();

    let _listener = registry
        .clone()
        .add_listener_local()
        .global(move |global| {
            let props = global.props.as_ref().unwrap();
            println!("{:#?}", foreign_dict_to_map(props));
            match global.type_ {
                ObjectType::Node => out_tx
                    .send(MessageOut::AddNode(NodeWrap {
                        id: global.id as usize,
                        // ui_attr_id: (global.id as usize) << 31,
                        media_type: {
                            props
                                .get("media.class")
                                .map(|class| {
                                    if class.contains("Audio") {
                                        MediaType::Audio
                                    } else if class.contains("Video") {
                                        MediaType::Video
                                    } else if class.contains("Midi") {
                                        MediaType::Midi
                                    } else {
                                        MediaType::Unknown
                                    }
                                })
                                .unwrap_or(MediaType::Unknown)
                        },
                        kind: {
                            let media_class = |class: &str| {
                                if class.contains("Sink") || class.contains("Input") {
                                    Some(NodeType::Input)
                                } else if class.contains("Source") || class.contains("Output") {
                                    Some(NodeType::Output)
                                } else {
                                    None
                                }
                            };
                            props
                                .get("media.category")
                                .and_then(|class| {
                                    if class.contains("Duplex") {
                                        None
                                    } else {
                                        props.get("media.class").and_then(media_class)
                                    }
                                })
                                .or_else(|| props.get("media.class").and_then(media_class))
                                .unwrap_or(NodeType::Unknown)
                        },
                        application: props.get("application.name").map(String::from),
                        name: String::from(
                            props
                                .get("node.nick")
                                .or_else(|| props.get("node.description"))
                                .or_else(|| props.get("node.name"))
                                .unwrap_or_else(|| "<unknown>"),
                        ),
                        ports: vec![],
                    }))
                    .unwrap(),
                ObjectType::Port => out_tx
                    .send(MessageOut::AddPort(PortWrap {
                        node: props
                            .get("node.id")
                            .expect("Port has no node.id property!")
                            .parse()
                            .expect("Could not parse node.id property"),
                        direction: if let Some("in") = props.get("port.direction") {
                            Direction::Input
                        } else {
                            Direction::Output
                        },
                        name: props
                            .get("port.name")
                            .unwrap_or_else(|| "<unknown>")
                            .to_string(),
                        id: global.id as usize,
                    }))
                    .unwrap(),
                ObjectType::Link => {
                    let proxy: Link = registry.bind(global).expect("Failed to bind to link proxy");
                    let out_tx2 = out_tx.clone();
                    let id = global.id as usize;
                    let listener = proxy
                        .add_listener_local()
                        .info(move |info| {
                            println!("{:#?}", foreign_dict_to_map(info.props().unwrap()));
                            out_tx2
                                .send(MessageOut::UpdateLink(LinkWrap {
                                    id,
                                    end: info.output_port_id() as usize,
                                    start: info.input_port_id() as usize,
                                    state: info.state().into(),
                                }))
                                .unwrap()
                        })
                        .register();
                    proxies.as_ref().borrow_mut().insert(id, (proxy, listener));
                }
                _ => {}
            }
        })
        .global_remove(move |global| out_tx2.send(MessageOut::Remove(global as usize)).unwrap())
        .register();

    let timer = mainloop.add_timer(move |_| {
        for ev in in_rx.try_iter() {
            println!("{ev:?}");
            match ev {
                MessageIn::RemoveLink(id) => {
                    if let Some((_proxy, _listener)) = proxies2.as_ref().borrow_mut().remove(&id) {
                        // core.destroy_object(proxy).unwrap();
                        registry2.destroy_global(id as u32);
                    }
                }
                MessageIn::CreateLink {
                    input_node,
                    input_port,
                    output_node,
                    output_port,
                } => {
                    core.create_object::<Link, _>(
                        "link-factory",
                        &properties! {
                            "link.output.node" => input_node.to_string(),
                            "link.output.port" => input_port.to_string(),
                            "link.input.node" => output_node.to_string(),
                            "link.input.port" => output_port.to_string(),
                            "object.linger" => "1"
                        },
                    )
                    .unwrap();
                }
            }
        }
    });
    timer
        .update_timer(
            Some(Duration::from_millis(20)),
            Some(Duration::from_millis(20)),
        )
        .into_result()?;

    mainloop.run();
    Ok(())
}

pub fn foreign_dict_to_map(fd: &ForeignDict) -> HashMap<String, String> {
    let mut h = HashMap::new();
    for k in fd.keys() {
        h.insert(k.to_owned(), fd.get(k).unwrap().to_owned());
    }
    h
}
