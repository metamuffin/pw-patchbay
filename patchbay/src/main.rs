#![feature(drain_filter)]
pub mod helper;
pub mod link;
pub mod node;
pub mod port;
pub mod pwconn;
pub mod ui;

use crossbeam_channel::{Receiver, Sender};
use egui::{Key, Ui};
use helper::dark_colors;
use link::LinkWrap;
use node::NodeWrap;
use port::PortWrap;
use pwconn::{pw, MessageIn, MessageOut};
use std::{collections::HashMap, process::exit, thread};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (out_tx, out_rx) = crossbeam_channel::unbounded();
    let (in_tx, in_rx) = crossbeam_channel::unbounded();

    thread::spawn(move || pw(out_tx, in_rx).unwrap());

    eframe::run_native(
        "pw-patchbay",
        eframe::NativeOptions::default(),
        Box::new(|cc| Box::new(App::new(cc, out_rx, in_tx))),
    );
}

struct App {
    message_in: Receiver<MessageOut>,
    message_out: Sender<MessageIn>,

    links: HashMap<usize, LinkWrap>,
    nodes: HashMap<usize, NodeWrap>,
    ports: HashMap<usize, PortWrap>,

    ctx: egui_nodes::Context,
}

impl App {
    fn new(
        cc: &eframe::CreationContext<'_>,
        out_rx: Receiver<MessageOut>,
        in_tx: Sender<MessageIn>,
    ) -> Self {
        let mut ctx = egui_nodes::Context::default();
        ctx.style = egui_nodes::Style {
            colors: dark_colors(),
            ..Default::default()
        };
        cc.egui_ctx.set_visuals(egui::Visuals::dark());
        Self {
            ctx,
            links: HashMap::new(),
            nodes: HashMap::new(),
            ports: HashMap::new(),
            message_out: in_tx,
            message_in: out_rx,
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        for ev in self.message_in.try_iter() {
            println!("{ev:?}");
            match ev {
                MessageOut::AddNode(node) => {
                    self.nodes.insert(node.id, node);
                }
                MessageOut::AddPort(port) => {
                    self.nodes.get_mut(&port.node).unwrap().ports.push(port.id);
                    self.ports.insert(port.id, port);
                }
                MessageOut::UpdateLink(link) => {
                    self.links.insert(link.id, link);
                }
                MessageOut::Remove(u) => {
                    self.links.remove(&u);
                    self.nodes.remove(&u);
                    self.ports.remove(&u);
                    for n in self.nodes.values_mut() {
                        n.ports.drain_filter(|e| *e == u);
                    }
                }
            }
        }

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    ui.menu_button("Options", |ui| {
                        ui.label("todo");
                    });
                    if ui.button("Quit").clicked() {
                        frame.quit();
                    }
                });
                self.patchbay_inner(ui)
            })
        });
    }
}

impl App {
    pub fn delete_selected_links(&mut self) {
        for link_id in self.ctx.get_selected_links() {
            self.message_out
                .send(MessageIn::RemoveLink(link_id))
                .unwrap()
        }
    }
}
