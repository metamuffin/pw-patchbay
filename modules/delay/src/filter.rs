use pipewire::{stream::StreamState, MainLoop, Properties, Loop};
use std::{
    ffi::{self, c_void, CString},
    mem, os,
    pin::Pin,
    ptr,
};

pub struct Filter<D> {
    pub ptr: ptr::NonNull<pipewire::sys::pw_filter>,
    pub keep_alive: FilterKeepAlive<D>,
}
pub enum FilterKeepAlive<D> {
    Simple {
        _events: Pin<Box<pipewire::sys::pw_filter_events>>,
        _data: Box<FilterListenerLocalCallbacks<D>>,
    },
    Nothing,
}

impl<D> Filter<D> {
    pub fn new(
        main_loop: &mut MainLoop,
        name: &str,
        properties: Properties,
        callbacks: FilterListenerLocalCallbacks<D>,
    ) -> Self {
        let name = CString::new(name).unwrap();
        let (events, data) = callbacks.into_raw();
        let data = Box::into_raw(data);
        let (filter, mut data) = unsafe {
            let filter = pipewire::sys::pw_filter_new_simple(
                main_loop.as_ptr(),
                name.as_ptr(),
                properties.into_raw(),
                events.as_ref().get_ref(),
                data as *mut _,
            );
            (filter, Box::from_raw(data))
        };
        let filter = ptr::NonNull::new(filter).unwrap();
        data.filter = Some(filter);

        Filter {
            ptr: filter,
            keep_alive: FilterKeepAlive::Simple {
                _events: events,
                _data: data,
            },
        }
    }
}

pub struct FilterListenerLocalCallbacks<D> {
    pub state_changed: Option<Box<dyn Fn(StreamState, StreamState)>>,
    pub control_info: Option<Box<dyn Fn(u32, *const pipewire::sys::pw_stream_control)>>,
    pub io_changed: Option<Box<dyn Fn(u32, *mut os::raw::c_void, u32)>>,
    pub param_changed: Option<Box<dyn Fn(u32, &mut D, *const libspa_sys::spa_pod)>>,
    pub command: Option<Box<dyn Fn(&mut D, *const libspa_sys::spa_command)>>,
    pub destroy: Option<Box<dyn Fn(&mut D)>>,
    pub add_buffer: Option<Box<dyn Fn(*mut pipewire::sys::pw_buffer)>>,
    pub remove_buffer: Option<Box<dyn Fn(*mut pipewire::sys::pw_buffer)>>,
    pub process: Option<Box<dyn Fn(&Filter<D>, &mut D)>>,
    pub drained: Option<Box<dyn Fn()>>,
    pub user_data: D,
    filter: Option<ptr::NonNull<pipewire::sys::pw_filter>>,
}

impl<D> FilterListenerLocalCallbacks<D> {
    pub fn new(user_data: D) -> Self {
        FilterListenerLocalCallbacks {
            command: Default::default(),
            destroy: Default::default(),
            process: Default::default(),
            filter: Default::default(),
            drained: Default::default(),
            add_buffer: Default::default(),
            control_info: Default::default(),
            io_changed: Default::default(),
            param_changed: Default::default(),
            remove_buffer: Default::default(),
            state_changed: Default::default(),
            user_data,
        }
    }

    pub fn into_raw(
        self,
    ) -> (
        Pin<Box<pipewire::sys::pw_filter_events>>,
        Box<FilterListenerLocalCallbacks<D>>,
    ) {
        let callbacks = Box::new(self);

        unsafe extern "C" fn on_state_changed<D>(
            data: *mut os::raw::c_void,
            old: pipewire::sys::pw_stream_state,
            new: pipewire::sys::pw_stream_state,
            error: *const os::raw::c_char,
        ) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_ref() {
                if let Some(ref cb) = state.state_changed {
                    let old = streamstate_from_raw(old, error);
                    let new = streamstate_from_raw(new, error);
                    cb(old, new)
                };
            }
        }

        unsafe extern "C" fn on_io_changed<D>(
            data: *mut os::raw::c_void,
            _: *mut c_void,
            id: u32,
            area: *mut os::raw::c_void,
            size: u32,
        ) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_ref() {
                if let Some(ref cb) = state.io_changed {
                    cb(id, area, size);
                }
            }
        }

        unsafe extern "C" fn on_param_changed<D>(
            data: *mut os::raw::c_void,
            _: *mut c_void,
            id: u32,
            param: *const libspa_sys::spa_pod,
        ) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_mut() {
                if let Some(ref cb) = state.param_changed {
                    cb(id, &mut state.user_data, param);
                }
            }
        }

        unsafe extern "C" fn on_add_buffer<D>(
            data: *mut std::os::raw::c_void,
            _: *mut c_void,
            buffer: *mut pipewire::sys::pw_buffer,
        ) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_ref() {
                if let Some(ref cb) = state.add_buffer {
                    cb(buffer);
                }
            }
        }

        unsafe extern "C" fn on_remove_buffer<D>(
            data: *mut std::os::raw::c_void,
            _: *mut c_void,
            buffer: *mut pipewire::sys::pw_buffer,
        ) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_ref() {
                if let Some(ref cb) = state.remove_buffer {
                    cb(buffer);
                }
            }
        }

        unsafe extern "C" fn on_process<D>(
            data: *mut ::std::os::raw::c_void,
            _position: *mut libspa_sys::spa_io_position,
        ) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_mut() {
                if let Some(ref cb) = state.process {
                    let stream = state
                        .filter
                        .map(|ptr| Filter {
                            ptr,
                            keep_alive: FilterKeepAlive::Nothing,
                        })
                        .expect("stream cannot be null");
                    cb(&stream, &mut state.user_data);
                }
            }
        }

        unsafe extern "C" fn on_drained<D>(data: *mut ::std::os::raw::c_void) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_ref() {
                if let Some(ref cb) = state.drained {
                    cb();
                }
            }
        }

        unsafe extern "C" fn on_command<D>(
            data: *mut std::os::raw::c_void,
            command: *const libspa_sys::spa_command,
        ) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_mut() {
                if let Some(ref cb) = state.command {
                    cb(&mut state.user_data, command);
                }
            }
        }

        unsafe extern "C" fn on_destroy<D>(data: *mut std::os::raw::c_void) {
            if let Some(state) = (data as *mut FilterListenerLocalCallbacks<D>).as_mut() {
                if let Some(ref cb) = state.destroy {
                    cb(&mut state.user_data);
                }
            }
        }

        let events = unsafe {
            let mut events: Pin<Box<pipewire::sys::pw_filter_events>> = Box::pin(mem::zeroed());
            events.version = pipewire::sys::PW_VERSION_STREAM_EVENTS;

            if callbacks.command.is_some() {
                events.command = Some(on_command::<D>);
            }
            if callbacks.destroy.is_some() {
                events.destroy = Some(on_destroy::<D>);
            }

            if callbacks.state_changed.is_some() {
                events.state_changed = Some(on_state_changed::<D>);
            }
            if callbacks.io_changed.is_some() {
                events.io_changed = Some(on_io_changed::<D>);
            }
            if callbacks.param_changed.is_some() {
                events.param_changed = Some(on_param_changed::<D>);
            }
            if callbacks.add_buffer.is_some() {
                events.add_buffer = Some(on_add_buffer::<D>);
            }
            if callbacks.remove_buffer.is_some() {
                events.remove_buffer = Some(on_remove_buffer::<D>);
            }
            if callbacks.process.is_some() {
                events.process = Some(on_process::<D>);
            }
            if callbacks.drained.is_some() {
                events.drained = Some(on_drained::<D>);
            }

            events
        };

        (events, callbacks)
    }
}
fn streamstate_from_raw(
    state: pipewire::sys::pw_stream_state,
    error: *const os::raw::c_char,
) -> StreamState {
    match state {
        pipewire::sys::pw_stream_state_PW_STREAM_STATE_UNCONNECTED => StreamState::Unconnected,
        pipewire::sys::pw_stream_state_PW_STREAM_STATE_CONNECTING => StreamState::Connecting,
        pipewire::sys::pw_stream_state_PW_STREAM_STATE_PAUSED => StreamState::Paused,
        pipewire::sys::pw_stream_state_PW_STREAM_STATE_STREAMING => StreamState::Streaming,
        _ => {
            let error = if error.is_null() {
                "".to_string()
            } else {
                unsafe { ffi::CStr::from_ptr(error).to_string_lossy().to_string() }
            };

            StreamState::Error(error)
        }
    }
}
