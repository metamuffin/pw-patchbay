pub mod filter;

use filter::{Filter, FilterListenerLocalCallbacks};
use pipewire::{
    prelude::ListenerBuilderT,
    properties, spa,
    stream::{ListenerLocalCallbacks, Stream},
    Context, Loop, MainLoop,
};
use std::{ffi::CString, mem, pin::Pin};

fn main() {
    pipewire::init();
    let mut mainloop = MainLoop::new().unwrap();
    let context = Context::new(&mainloop).unwrap();
    let core = context.connect(None).unwrap();
    let registry = core.get_registry().unwrap();

    let props = properties! {
        *pipewire::keys::MEDIA_TYPE => "Audio",
        *pipewire::keys::MEDIA_CATEGORY => "Filter",
        *pipewire::keys::MEDIA_ROLE => "DSP",
    };

    let callbacks = FilterListenerLocalCallbacks::new(());

    let filter = Filter::new(
        &mut mainloop,
        "Hello",
        props,
        callbacks,
    );

    mainloop.run();
    unsafe { pipewire::deinit() };
}
